"""earwig URL Configuration
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from .urls_utils import decorated_includes, manager_required
from .manager.urls import urlpatterns as manager_urls

from . import views

urlpatterns = [
    url(r'^$', views.homepage),
    url(r'^sounds/(?P<pk>\d+)/image/$', views.sound_image, name='sound-image'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^manage/', decorated_includes(manager_required, include(manager_urls))),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
