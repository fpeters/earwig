# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.views.generic import ListView, DetailView

from sorl.thumbnail import get_thumbnail

from .sounds.models import Sound


class Homepage(ListView):
    model = Sound
    template_name = 'earwig/home.html'

    def get_queryset(self):
        qs = super(Homepage, self).get_queryset().select_related()
        qs = qs.filter(status='published')
        query = self.request.GET.get('q')
        if query:
            qs = qs.filter(Q(title__icontains=query) |
                           Q(description__icontains=query) |
                           Q(channel__title__icontains=query))
        else:
            qs = qs.order_by('?')  # random selection
        return qs

homepage = Homepage.as_view()


class SoundImage(DetailView):
    model = Sound

    def get(self, *args, **kwargs):
        image_url = self.get_object().image_url or self.get_object().channel.image_url
        if not image_url:
            return HttpResponseNotFound()
        im = get_thumbnail(image_url, '300x300', crop='center')
        return HttpResponseRedirect(im.url)

sound_image = SoundImage.as_view()
