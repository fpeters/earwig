# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.homepage, name='earwig-manager-homepage'),
    url(r'^channels/$', views.channels, name='earwig-channels'),
    url(r'^channels/add/$', views.channel_add, name='earwig-channel-add'),
    url(r'^channels/(?P<pk>\d+)/$', views.channel_detail, name='earwig-channel-detail'),
    url(r'^channels/(?P<pk>\d+)/edit/$', views.channel_edit, name='earwig-channel-edit'),
    url(r'^channels/(?P<pk>\d+)/delete/$', views.channel_delete, name='earwig-channel-delete'),
    url(r'^sound/(?P<pk>\d+)/$', views.sound_detail, name='earwig-sound-detail'),
    url(r'^sound/(?P<pk>\d+)/publish/$', views.sound_publish, name='earwig-sound-publish'),
    url(r'^sound/(?P<pk>\d+)/unpublish/$', views.sound_unpublish, name='earwig-sound-unpublish'),
    url(r'^sound/(?P<pk>\d+)/reject/$', views.sound_reject, name='earwig-sound-reject'),
]
