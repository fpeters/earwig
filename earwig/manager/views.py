# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.db.models import Q
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView

from ..sounds.models import Channel, Sound, STATUSES


class Homepage(ListView):
    model = Sound
    paginate_by = 10
    template_name = 'earwig/manager_home.html'

    def get_queryset(self, **kwargs):
        qs = self.model.objects.select_related()
        if self.get_status_filter() != '_all':
            qs = qs.filter(status=self.get_status_filter())
        query = self.get_query_filter()
        if query:
            qs = qs.filter(Q(title__icontains=query) |
                           Q(channel__title__icontains=query))
        return qs

    def get_query_filter(self):
        return self.request.GET.get('q') or ''

    def get_status_filter(self):
        return self.request.GET.get('q_status') or ''

    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)
        context['statuses'] = STATUSES
        context['next'] = reverse_lazy('earwig-manager-homepage')
        return context

homepage = Homepage.as_view()


class Channels(ListView):
    model = Channel
    paginate_by = 20
    template_name = 'earwig/manager_channels.html'

channels = Channels.as_view()


class ChannelDetailView(DetailView):
    model = Channel
    template_name = 'earwig/manager_channel.html'

    def get_sounds_queryset(self, **kwargs):
        qs = self.get_object().sound_set.select_related()
        if self.get_status_filter() != '_all':
            qs = qs.filter(status=self.get_status_filter())
        query = self.get_query_filter()
        if query:
            qs = qs.filter(Q(title__icontains=query) |
                           Q(description__icontains=query))
        return qs

    def get_query_filter(self):
        return self.request.GET.get('q') or ''

    def get_status_filter(self):
        return self.request.GET.get('q_status') or '_all'

    def get_context_data(self, **kwargs):
        context = super(ChannelDetailView, self).get_context_data()
        context['object_list'] = self.get_sounds_queryset()
        context['next'] = reverse_lazy('earwig-channel-detail', kwargs={'pk': self.get_object().pk})
        context['statuses'] = STATUSES
        return context

channel_detail = ChannelDetailView.as_view()


class ChannelAdd(CreateView):
    model = Channel
    fields = ('title', 'channel_url', 'feed_url', 'policy')
    template_name = 'earwig/manager_channel_add.html'
    success_url = reverse_lazy('earwig-channels')

channel_add = ChannelAdd.as_view()


class ChannelEdit(UpdateView):
    model = Channel
    fields = ('title', 'channel_url', 'feed_url', 'policy')
    template_name = 'earwig/manager_channel_edit.html'

    def get_success_url(self):
        return reverse_lazy('earwig-channel', kwargs={'pk': self.get_object().pk})

channel_edit = ChannelEdit.as_view()

class ChannelDeleteView(DeleteView):
    model = Channel
    template_name = 'earwig/manager_channel_confirm_delete.html'
    success_url = reverse_lazy('earwig-channels')

channel_delete = ChannelDeleteView.as_view()


class SoundDetailView(DetailView):
    model = Sound
    template_name = 'earwig/manager_sound.html'

sound_detail = SoundDetailView.as_view()


class SoundActionView(DetailView):
    model = Sound

    def get_success_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse_lazy('earwig-sound-detail', kwargs={'pk': self.get_object().pk})


class SoundPublishView(SoundActionView):
    def get(self, *args, **kwargs):
        self.get_object().publish()
        messages.success(self.request, _('%s has been published.') % self.get_object())
        return redirect(self.get_success_url())

sound_publish = SoundPublishView.as_view()


class SoundUnpublishView(SoundActionView):
    def get(self, *args, **kwargs):
        self.get_object().unpublish()
        messages.success(self.request, _('%s has been unpublished.') % self.get_object())
        return redirect(self.get_success_url())

sound_unpublish = SoundUnpublishView.as_view()


class SoundRejectView(SoundActionView):
    def get(self, *args, **kwargs):
        self.get_object().reject()
        messages.warning(self.request, _('%s has been rejected.') % self.get_object())
        return redirect(self.get_success_url())

sound_reject = SoundRejectView.as_view()
