# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .fields import RichTextField


POLICIES = [
    ('', _('Default (moderate)')),
    ('publish', _('Autopublish')),
]

STATUSES = [
    ('', _('Waiting for moderation')),
    ('published', _('Published')),
    ('unpublished', _('Unpublished')),
    ('rejected', _('Rejected')),
]


class Channel(models.Model):
    channel_url = models.URLField(_('Channel URL'))
    title = models.CharField(_('Title'), max_length=200)
    feed_url = models.URLField(_('Feed URL'), blank=True)
    policy = models.CharField(_('Policy'), max_length=20,
            default='', blank=True, choices=POLICIES)

    subtitle = models.CharField(_('Subtitle'), max_length=500, blank=True, null=True)
    image_url = models.URLField(_('Image URL'), blank=True, null=True)
    license_url = models.URLField(_('License URL'), blank=True, null=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)
    last_check_timestamp = models.DateTimeField(null=True)

    class Meta:
        ordering = ('title',)

    def __str__(self):
        return self.title

    def update(self):
        from . import sources
        source = sources.Rss(self)
        source.update_meta()
        source.update_sounds()


class Sound(models.Model):
    sound_url = models.URLField(_('Sound URL'))
    title = models.CharField(_('Title'), max_length=200)
    description = RichTextField(_('Description'), blank=True, null=True)
    page_url = models.URLField(_('Page URL'), blank=True)
    uuid = models.CharField(_('Unique Identifier'), max_length=200, blank=True, null=True)
    channel = models.ForeignKey('Channel', on_delete=models.SET_NULL, blank=True, null=True)

    original_publication_date = models.DateTimeField(
            _('Original Publication Date'), blank=True, null=True)
    file_type = models.CharField(_('File Type'), max_length=20, blank=True, null=True)
    file_size = models.BigIntegerField(_('File Size'), blank=True, null=True)
    duration = models.IntegerField(_('Duration'), blank=True, null=True)  # in seconds
    image_url = models.URLField(_('Image URL'), blank=True, null=True)

    status = models.CharField(_('Publication Status'), max_length=20,
            default='', blank=True, choices=STATUSES)
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('channel', 'original_publication_date', 'title')
        indexes = [models.Index(fields=['status'])]

    def can_publish(self):
        return self.status != 'published'

    def can_reject(self):
        return self.status in ('', 'published')

    def can_unpublish(self):
        return self.status == 'published'

    def status_label(self):
        return dict(STATUSES).get(self.status)

    def publish(self):
        self.status = 'published'
        self.save()

    def unpublish(self):
        self.status = 'unpublished'
        self.save()

    def reject(self):
        self.status = 'rejected'
        self.save()

    def __str__(self):
        return self.title
