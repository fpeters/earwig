# -*- coding: utf-8 -*-
#
# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Originally from Combo, (AGPL as well),
# <http://git.entrouvert.org/combo.git/tree/combo/data/fields.py>

from django.conf import settings

import ckeditor.fields


class RichTextField(ckeditor.fields.RichTextField):
    def formfield(self, **kwargs):
        defaults = {
            'form_class': RichTextFormField,
            'config_name': self.config_name,
            'extra_plugins' : self.extra_plugins,
            'external_plugin_resources': self.external_plugin_resources
        }
        defaults.update(kwargs)
        return super(RichTextField, self).formfield(**defaults)


class RichTextFormField(ckeditor.fields.RichTextFormField):
    def clean(self, value):
        value = super(RichTextFormField, self).clean(value)
        if settings.LANGUAGE_CODE.startswith('fr-'):
            # apply some typographic rules
            value = value.replace(u'&laquo; ', u'«\u202f')
            value = value.replace(u'« ', u'«\u202f')
            value = value.replace(u' &raquo;', u'\u202f»')
            value = value.replace(u' »', u'\u202f»')
            value = value.replace(u' :', u'\u00a0:')
            value = value.replace(u' ;', u'\u202f;')
            value = value.replace(u' !', u'\u202f!')
            value = value.replace(u' ?', u'\u202f?')
        return value
