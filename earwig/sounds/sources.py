# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.utils.timezone import make_aware
import feedparser

from .models import Sound


class Rss(object):
    def __init__(self, channel):
        self.channel = channel
        assert channel.feed_url

    def update_meta(self):
        feed = feedparser.parse(self.channel.feed_url)
        attrs = {
            'subtitle': getattr(feed.feed, 'subtitle', None),
            'image_url': feed.feed.image.href if getattr(feed.feed, 'image', None) and feed.feed.image.href else None,
            'license_url': getattr(feed.feed, 'license', None),
        }
        modified = False
        for key, value in attrs.items():
            if getattr(self.channel, key) != value:
                setattr(self.channel, key, value)
                modified = True
        if modified:
            self.channel.save()

    def update_sounds(self):
        feed = feedparser.parse(self.channel.feed_url)
        for entry in feed.entries:
            for link in entry.links:
                if link.rel != 'enclosure':
                    continue
                if not link.type.startswith('audio/'):
                    continue
                sound_url = link.href
                sound, created = Sound.objects.get_or_create(uuid=entry.id,
                        defaults={
                            'title': entry.title,
                            'sound_url': sound_url})
                sound.title = entry.title
                sound.page_url = entry.link
                sound.channel = self.channel
                sound.file_type = link.type
                sound.image_url = entry.image.href if hasattr(entry, 'image') and entry.image.href else None
                assert entry.description_detail.get('type') == 'text/html'
                sound.description = entry.description_detail['value']
                sound.original_publication_date = make_aware(datetime.datetime(*entry.published_parsed[:6]))
                link_length = int(link.length) if link.length else None
                if link_length != sound.file_size:
                    sound.file_size = link_length
                    sound.duration = None  # reset
                if sound_url != sound.sound_url:
                    sound.sound_url = sound_url
                    sound.duration = None  # reset
                if created and self.channel.policy == 'publish':
                    sound.status = 'published'
                sound.save()
