# earwig
# Copyright (C) 2018  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import sys

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils.timezone import now

from ...models import Channel


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--channel', dest='channel', metavar='CHANNEL', type=str)
        parser.add_argument('--delay', dest='delay', metavar='DELAY',
                type=int, default=3600)

    def handle(self, *args, **options):
        qs = Channel.objects.all()
        if options.get('channel'):
            qs = qs.filter(Q(title__icontains=options.get('channel')) |
                           Q(channel_url__icontains=options.get('channel')))
        if options.get('delay'):
            qs = qs.filter(Q(last_check_timestamp__isnull=True) |
                           Q(last_check_timestamp__lt=now() -
                             datetime.timedelta(seconds=options.get('delay'))))
        for channel in qs:
            if options.get('verbosity'):
                sys.stderr.write('Updating %s\n' % channel)
            channel.update()
            channel.last_check_timestamp = now()
            channel.save(update_fields=['last_check_timestamp'])
