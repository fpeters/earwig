$(function() {
  $('#content').delegate('div.tile a', 'click', function() {
    $('header').removeClass('hidden');
    $('header #cover').css('background-image', $(this).parent().css('background-image'));
    $('header #title').text($(this).find('.channel').text() + ' - ' + $(this).find('.sound').text());
    $('header #site').attr('href', $(this).data('page-uri'));
    $('audio').empty();
    var source = $('<source>', {src: $(this).data('sound-uri')});
    $('audio').append(source);
    $('audio')[0].pause();
    $('audio')[0].load();
    $('audio')[0].play();
  });
  $('#filter').on('submit', function() {
    $.ajax({
      url: '/',
      data: {q: $('#filter input').val()}
    }).done(function(result) {
      $('#content').html($(result).find('div.tile'));
    });
    return false;
  });
});
