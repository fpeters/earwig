# Earwig

Sound Aggregator.


## Installation

(one of many possibilities)

    git clone https://git.0d.be/g/earwig.git
    virtualenv -p python3 venv3 --system-site-packages
    . venv3/bin/activate
    cd earwig
    python setup.py develop
    python setup.py compile_scss
    ./manage.py migrate
    ./manage.py compilemessages
    ./manage.py createsuperuser
    ./manage.py runserver


## License

Earwig is licensed under the [GNU Affero General Public License v3.0](http://www.gnu.org/)
or later.
